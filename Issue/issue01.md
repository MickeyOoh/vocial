result: ** This is not a problem **
I succeeded login process I mistook as it is a problem


test/vocial_web/controllers/poll_controller_test.exs
```
  defp login(conn, user) do 
    IO.puts "conn => #{inspect conn}"
    conn
    |> post("/sessions", %{username: user.username,password: user.password})
    |> IO.inspect
  end
```

result of login
conn =>
  %Plug.Conn{
    adapter: {Plug.Adapters.Test.Conn, :...},
    assigns: %{},
    before_send: [],
    body_params: %Plug.Conn.Unfetched{aspect: :body_params},
    cookies: %Plug.Conn.Unfetched{aspect: :cookies},
    halted: false,
    host: "www.example.com",
    method: "GET",
    owner: #PID<0.339.0>,
    params: %Plug.Conn.Unfetched{aspect: :params},
    path_info: [],
    path_params: %{},
    port: 80,
    private: %{phoenix_recycled: true, plug_skip_csrf_protection: true},
    query_params: %Plug.Conn.Unfetched{aspect: :query_params},
    query_string: "",
    remote_ip: {127, 0, 0, 1},
    req_cookies: %Plug.Conn.Unfetched{aspect: :cookies},
    req_headers: [],
    request_path: "/",
    resp_body: nil,
    resp_cookies: %{},
    resp_headers: [{"cache-control", "max-age=0, private, must-revalidate"}],
    scheme: :http,
    script_name: [],
    secret_key_base: nil,
    state: :unset,
    status: nil}

%Plug.Conn{
  adapter: {Plug.Adapters.Test.Conn, :...},
  assigns: %{},
  before_send: [#Function<0.24896844/1 in Plug.CSRFProtection.call/2>,
   #Function<4.23722417/1 in Phoenix.Controller.fetch_flash/2>,
   #Function<0.45862765/1 in Plug.Session.before_send/2>,
   #Function<1.6754055/1 in Plug.Logger.call/2>],
  body_params: %{"password" => "test", "username" => "test"},
  cookies: %{
    "_vocial_key" => "SFMyNTY.g3QAAAACbQAAAA1waG9lbml4X2ZsYXNodAAAAAFtAAAABGluZm9tAAAAF0xvZ2dlZCBpbiBzdWNjZXNzZnVsbHkhbQAAAAR1c2VydAAAAANkAAVlbWFpbG0AAAANdGVzdEB0ZXN0LmNvbWQAAmlkYgAABbhkAAh1c2VybmFtZW0AAAAEdGVzdA.dirhkEMB5Pcedeiw5bcelpYptDuneTuKpJrWMdF8bM4"
  },
  halted: false,
  host: "www.example.com",
  method: "POST",
  owner: #PID<0.339.0>,
  params: %{"password" => "test", "username" => "test"},
  path_info: ["sessions"],
  path_params: %{},
  port: 80,
  private: %{
    VocialWeb.Router => {[], %{}},
    :phoenix_action => :create,
    :phoenix_controller => VocialWeb.SessionController,
    :phoenix_endpoint => VocialWeb.Endpoint,
    :phoenix_flash => %{"info" => "Logged in successfully!"},
    :phoenix_format => "html",
    :phoenix_layout => {VocialWeb.LayoutView, :app},
    :phoenix_pipelines => [:browser],
    :phoenix_recycled => false,
    :phoenix_router => VocialWeb.Router,
    :phoenix_view => VocialWeb.SessionView,
    :plug_session => %{
      "phoenix_flash" => %{"info" => "Logged in successfully!"},
      "user" => %{email: "test@test.com", id: 1464, username: "test"}
    },
    :plug_session_fetch => :done,
    :plug_session_info => :write,
    :plug_skip_csrf_protection => true
  },
  query_params: %{},
  query_string: "",
  remote_ip: {127, 0, 0, 1},
  req_cookies: %{},
  req_headers: [{"content-type", "multipart/mixed; boundary=plug_conn_test"}],
  request_path: "/sessions",
  resp_body: "<html><body>You are being <a href=\"/\">redirected</a>.</body></html>",
  resp_cookies: %{
    "_vocial_key" => %{
      value: "SFMyNTY.g3QAAAACbQAAAA1waG9lbml4X2ZsYXNodAAAAAFtAAAABGluZm9tAAAAF0xvZ2dlZCBpbiBzdWNjZXNzZnVsbHkhbQAAAAR1c2VydAAAAANkAAVlbWFpbG0AAAANdGVzdEB0ZXN0LmNvbWQAAmlkYgAABbhkAAh1c2VybmFtZW0AAAAEdGVzdA.dirhkEMB5Pcedeiw5bcelpYptDuneTuKpJrWMdF8bM4"
    }
  },
  resp_headers: [
    {"set-cookie",
     "_vocial_key=SFMyNTY.g3QAAAACbQAAAA1waG9lbml4X2ZsYXNodAAAAAFtAAAABGluZm9tAAAAF0xvZ2dlZCBpbiBzdWNjZXNzZnVsbHkhbQAAAAR1c2VydAAAAANkAAVlbWFpbG0AAAANdGVzdEB0ZXN0LmNvbWQAAmlkYgAABbhkAAh1c2VybmFtZW0AAAAEdGVzdA.dirhkEMB5Pcedeiw5bcelpYptDuneTuKpJrWMdF8bM4; path=/; HttpOnly"},
    {"content-type", "text/html; charset=utf-8"},
    {"cache-control", "max-age=0, private, must-revalidate"},
    {"x-frame-options", "SAMEORIGIN"},
    {"x-xss-protection", "1; mode=block"},
    {"x-content-type-options", "nosniff"},
    {"x-download-options", "noopen"},
    {"x-permitted-cross-domain-policies", "none"},
    {"location", "/"}
  ],
  scheme: :http,
  script_name: [],
  secret_key_base: "FTwcG7GCCqVV32/PyouMGNGpnVu4DKRTtcjCFDfSmRHbAuvtY1xmRVz16+BXe/8P",
  state: :sent,
  status: 302
}

