RESOLVED

Cause:
 there are 2 create functions and the order was wrong.
 So create is called repeatedly endlessly.
 Changed the order 



warning: variable "option_id" is unused
  test/vocial_web/channels/polls_channel_test.exs:30

warning: variable "votes" is unused
  test/vocial_web/channels/polls_channel_test.exs:30

.........14:14:11.276 [error] Postgrex.Protocol (#PID<0.295.0>) disconnected: ** (DBConnection.ConnectionError) owner #PID<0.399.0> timed out because it owned the connection for longer than 15000ms


  1) test POST /polls (with valid data) (VocialWeb.PollControllerTest)
     test/vocial_web/controllers/poll_controller_test.exs:45
     ** (ExUnit.TimeoutError) test timed out after 60000ms. You can change the timeout:
     
       1. per test by setting "@tag timeout: x"
       2. per case by setting "@moduletag timeout: x"
       3. globally via "ExUnit.start(timeout: x)" configuration
       4. or set it to infinity per run by calling "mix test --trace"
          (useful when using IEx.pry)
     
     Timeouts are given as integers in milliseconds.
     
     code: |> post("/polls", %{"poll" => %{"title" => "Test Poll"},
     stacktrace:
       (vocial) lib/vocial_web/controllers/poll_controller.ex:22: VocialWeb.PollController.create/2
       (vocial) lib/vocial_web/controllers/poll_controller.ex:1: VocialWeb.PollController.action/2
       (vocial) lib/vocial_web/controllers/poll_controller.ex:1: VocialWeb.PollController.phoenix_controller_pipeline/2
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.instrument/4
       (phoenix) lib/phoenix/router.ex:278: Phoenix.Router.__call__/1
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.plug_builder_call/2
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.call/2
       (phoenix) lib/phoenix/test/conn_test.ex:224: Phoenix.ConnTest.dispatch/5
       test/vocial_web/controllers/poll_controller_test.exs:47: (test)
       (ex_unit) lib/ex_unit/runner.ex:306: ExUnit.Runner.exec_test/1
       (stdlib) timer.erl:166: :timer.tc/1
       (ex_unit) lib/ex_unit/runner.ex:245: anonymous fn/4 in ExUnit.Runner.spawn_test/3

.voter_ip => "127.0.0.1"
.....14:15:13.693 [error] Postgrex.Protocol (#PID<0.298.0>) disconnected: ** (DBConnection.ConnectionError) owner #PID<0.419.0> timed out because it owned the connection for longer than 15000ms


  2) test POST /polls (with invalid data) (VocialWeb.PollControllerTest)
     test/vocial_web/controllers/poll_controller_test.exs:52
     ** (ExUnit.TimeoutError) test timed out after 60000ms. You can change the timeout:
     
       1. per test by setting "@tag timeout: x"
       2. per case by setting "@moduletag timeout: x"
       3. globally via "ExUnit.start(timeout: x)" configuration
       4. or set it to infinity per run by calling "mix test --trace"
          (useful when using IEx.pry)
     
     Timeouts are given as integers in milliseconds.
     
     code: |> post("/polls", %{"poll" => %{title: nil}, "options" => "One,Two,Three"})
     stacktrace:
       (vocial) lib/vocial_web/controllers/poll_controller.ex:22: VocialWeb.PollController.create/2
       (vocial) lib/vocial_web/controllers/poll_controller.ex:1: VocialWeb.PollController.action/2
       (vocial) lib/vocial_web/controllers/poll_controller.ex:1: VocialWeb.PollController.phoenix_controller_pipeline/2
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.instrument/4
       (phoenix) lib/phoenix/router.ex:278: Phoenix.Router.__call__/1
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.plug_builder_call/2
       (vocial) lib/vocial_web/endpoint.ex:1: VocialWeb.Endpoint.call/2
       (phoenix) lib/phoenix/test/conn_test.ex:224: Phoenix.ConnTest.dispatch/5
       test/vocial_web/controllers/poll_controller_test.exs:54: (test)
       (ex_unit) lib/ex_unit/runner.ex:306: ExUnit.Runner.exec_test/1
       (stdlib) timer.erl:166: :timer.tc/1
       (ex_unit) lib/ex_unit/runner.ex:245: anonymous fn/4 in ExUnit.Runner.spawn_test/3

.............

  3) test polls list_polls/0 returns all polls (Vocial.VotesTest)
     test/vocial/votes/votes_test.exs:45
     Assertion with == failed
     code:  assert Votes.list_polls() == [poll]
     left:  [%Vocial.Votes.Poll{__meta__: #Ecto.Schema.Metadata<:loaded, "polls">, id: 866, inserted_at: ~N[2018-08-28 18:16:02.592464], options: [], title: "Hello", updated_at: ~N[2018-08-28 18:16:02.592469], user: #Ecto.Association.NotLoaded<association :user is not loaded>, user_id: 1424, image: nil, vote_records: []}]
     right: [%Vocial.Votes.Poll{__meta__: #Ecto.Schema.Metadata<:loaded, "polls">, id: 866, inserted_at: ~N[2018-08-28 18:16:02.592464], options: [], title: "Hello", updated_at: ~N[2018-08-28 18:16:02.592469], user: #Ecto.Association.NotLoaded<association :user is not loaded>, user_id: 1424, image: #Ecto.Association.NotLoaded<association :image is not loaded>, vote_records: #Ecto.Association.NotLoaded<association :vote_records is not loaded>}]
     stacktrace:
       test/vocial/votes/votes_test.exs:47: (test)

...

  4) test polls show_poll/1 returns a specific poll (Vocial.VotesTest)
     test/vocial/votes/votes_test.exs:82
     Assertion with == failed
     code:  assert Votes.get_poll(poll.id()) == poll
     left:  %Vocial.Votes.Poll{__meta__: #Ecto.Schema.Metadata<:loaded, "polls">, id: 869, inserted_at: ~N[2018-08-28 18:16:03.944917], options: [], title: "Hello", updated_at: ~N[2018-08-28 18:16:03.944926], user: #Ecto.Association.NotLoaded<association :user is not loaded>, user_id: 1428, image: nil, vote_records: []}
     right: %Vocial.Votes.Poll{__meta__: #Ecto.Schema.Metadata<:loaded, "polls">, id: 869, inserted_at: ~N[2018-08-28 18:16:03.944917], options: [], title: "Hello", updated_at: ~N[2018-08-28 18:16:03.944926], user: #Ecto.Association.NotLoaded<association :user is not loaded>, user_id: 1428, image: #Ecto.Association.NotLoaded<association :image is not loaded>, vote_records: #Ecto.Association.NotLoaded<association :vote_records is not loaded>}
     stacktrace:
       test/vocial/votes/votes_test.exs:84: (test)

.....

Finished in 132.4 seconds
40 tests, 4 failures

Randomized with seed 845323
vocial$ 

