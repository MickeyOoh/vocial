# Vocial

Creating a poll controller
----

Building the poll controller
----

We'll start off by creating a new file, `/lib/vocial_web/controllers/poll_controller.ex`. 

```
defmodule VocialWeb.PollController do 
  use VocialWeb, :controller

  def index(conn, _params) do 
    render conn, "index.html"
  end
end
```

Let's create `lib/vocial_web/view/poll_view.ex`
```
defmodule VocialWeb.PollView do 
  use VocialWeb, :view
end
```






Understanding Ecto query writing
------

```
iex> h Vocial.Repo.all



iex> Repo.all(Poll)
[debug] QUERY OK source="polls" db=1.0ms
SELECT p0."id", p0."title", p0."inserted_at", p0."updated_at" FROM "polls" AS p0 []
[
  %Vocial.Votes.Poll{
    __meta__: #Ecto.Schema.Metadata<:loaded, "polls">,
    id: 1,
    inserted_at: ~N[2018-08-08 23:01:13.330629],
    options: #Ecto.Association.NotLoaded<association :options is not loaded>,
    title: "Sample Poll",
    updated_at: ~N[2018-08-08 23:01:13.341534]
  }
]
iex(25)> Repo.all(from p in Poll)
** (CompileError) iex:25: undefined function from/1
```

> ISSUE1: 'Repo.all(from p in Poll)' doesn't work and error occurred.

**Result** we need to import Ecto.Query
```
iex> import Ecto.Query
iex> alias Vocial.Repo
iex> alias Vocial.Votes.Poll
iex> Repo.all(Poll)


iex> Repo.all(from p in Poll)
[debug] QUERY OK source="polls" db=3.5ms
SELECT p0."id", p0."title", p0."inserted_at", p0."updated_at" FROM "polls" AS p0 []
[
  %Vocial.Votes.Poll{
    __meta__: #Ecto.Schema.Metadata<:loaded, "polls">,
    id: 1,
    inserted_at: ~N[2018-08-08 23:01:13.330629],
    options: #Ecto.Association.NotLoaded<association :options is not loaded>,
    title: "Sample Poll",
    updated_at: ~N[2018-08-08 23:01:13.341534]
  }
]
```

Creating our create function
----

> Issue wanring code:
```
def create(conn, %{"poll" => poll_params, "options" => options}) do 
  split_options = String.split(options, ",")
  with {:ok, poll} <- Votes.create_poll_with_options(poll_params, split_options) do 
    conn
    |> put_flash(:info, "Poll created successfully!")
    |> redirect(to: poll_path(conn, :index))
  end
end
warning: variable /"poll/" is unused
  lib/vocial_web/controllers/poll_controller.ex:17
```

> Issue : 

Stroing and Retrieving Vote Data with Ecto Pages
-----

### Writing our unit tests
----
> Issue : test "GET /polls" failed

```
assert html_request(conn, 200) =~ poll.title

poll.title = "Poll 1"
left = <title>Hello Vocial!</title>  ==> failed
```

result: code index.html.eex doesn't have no code although it is a file.
  I put codes into layout/special.thml.eex instead of index.thml.eex.
  So title which test code set in are not affected to index.html.eex, that's
  why the above issue happenned.


> Issue : Postgresql tables
    vocial_dev
    vocial_test
vocial_dev has one record containing "Sample xxxxx" in vocial_dev table.
but no data in vocial_test
After all, what parts of code writes data into table and which table it chooses.
"mix test" case it would selects "vocial_test" table and project selects "vocial_dev".
**I miss the alogrithm of selecting table and writ data**.


Introducing User Accounts and Sessions
----

This chapter covers
* Adding user accounts to our application
* Setting up the database to support users
* Adding encryption to database fields
* Writing tests to support user logic
* Introducing basic login systems

### Adding user accounts
----

1. figure out what the database table should look like
1. Figure out what the code representation shoudl look like
1. Determine if the addition of this table should introduce changes into other
   models or schemas
1. Write the migration code that introduce the new table
1. Write the migration code that modifies existing tables and data(if necessary)
1. Modify an existing context or create a new context
1. Create a new schema
1. Modify existing schemas that are affected by this addition
1. Tie the new context logic back into our controllers
1. Write tests for the database side of things
1. Write tests for the controller and request sides of things

### Designing our user schema
----
**What** information we want to store:
```
Users
----
-* id
- username
- email
- encrypted_password
- active
-* inserted_at
-* updated_at
```



### Creating a user signup page
----

* Create the route that will tell the browser how to get to the right pages
* Create teh controller and the functions in the controller that will tell Phoexnix how to handle the routes
* Create the templates that will represent the actual interface that the user sees
* Hook the context up to each of the functions
* Write tests that actually cover all of this new functionality that we're adding ovet time

|--------|     |----------------|     |-------------|     |---------------|
|  user  | --> | GET /users/new | --> | POST /users | --> | GET /users/id |
|--------|     |----------------|     |-------------|     |---------------|



### Installing Comeonin
----


Vaidatons, Errors, and Tying Loose Ends
-----

* The different validation methods available in Ecto
* Building more migrations
* Working with Ecto associations
* Fixing tests
* Sending information from Ecto to our controllers and views

### Connecting polls to users
----
1. We need to link our tables by crating a migration that adds *user_id* to our *polls* table
1. We then need to moidfy the code in our schemas to represent the addtion of the association.
1. We need to modify all of the creation code in the context to set the poll's *user_id* to the appropriate user.
1. Finally, we need to modify all of the *delete* and *update* code to only allow those functions to happen if the current user is the owner of that pol.

#### Sending a user ID through the controller
----

##### Retreving data from sessions
----

##### Writing our Poll Controller's tests
-----
We need to start off by creating a new file to handle the tests for 
our Poll Controller. Open up `test/vocial_web/controller/poll_test.exs` 
and begin to update the tests in it.

We should already have a user created and ready to go for our tests, so
Let's now create a setup block that will create a new user account.
Remember that the syntax for creating helper syntax blocks requires a block
that returns a tuple with `:ok` as the first value and whatever data we want
to expose to each of our tests as the second value. This should be added to
the `poll_controller_test.exs` file that we were working within the 
preceding snippet.
```
setup do 


end
```
In the previous snippet, we created a valid sample user that we can use
as part of our association process when creating new polls. This will
give us a pretty good test of the logic in our controller by marking
sure that there is at least one valid user in the system. We should now
create our starting `conn` object, as that is a necessary part of 
every controller test. Another thing we'll need  is a way to simulate a
logged-in user; the creation of the user account itself isn't enough
new private login function that will take in the `conn` and the existing
user, shown as follow:

```
def login(conn, user) do 

end
```


#### Restricting access via sessions
---

> Issue: after adding `verify_user_session.ex` and running server, "You must login " message doesn't come out.
> result: the book was wrong to tell us the wrong url `localhost:4000`
correct one is `localhost:4000/polls/new`

Issue: At the last of this, `mix test` show up the error
   1) test DELETE /sessions (VocialWeb.SessionControllerTest)
      ** (Phoenix.Router.NoRouteError) no route found for DELETE /logout ...

result: First time I changed `get` to `delete` in router.ex as I thought the book was wrong. Then I changed `delete` to `get` to return back the book.
 But after all the book was wrong as I am right. I have to change it again.
 `delete` is correct as I believed first.

### Working with validations and errors
----
Ecto provides a number of built-in changeset validations that we can
hook into to ensure the data integrity of an application. Out of the box,
Ecto provides the following validations to use:
* validate_change
* validate_confirmation
* validate_format
* validate_length
* validate_number
* validate_required
* unique_constraint

#### Making usernames unique
----
At the moment, multiple people can register with the same username, 
something that would inevitably cause a lot of problems in a real-world
situation. Ecto does not allow us to verify the username's uniqueness 
without there being a database-level constraint check, so let's start by
creating a migration to make usernames unique in the database.

```
$ mix ecto.gen.migration make_usernames_unique

```


Live Voting with Phoenix
----
Web Sockets are means of passing data back and forth through a dedicated(or
socket) that allows for real-time communication back and forth between a
client(your web browser) and the server(your Phoenix applicatoin). This 
information can either be solely limited to comunication with a single 
client or multiple clients simultaneously(a boradcast message, similar to 
chat rooms). This allows the construction of real-time applications where
the server is the results back out ot all listening parties.

It's important to note that this specifcally works via constructing a client
that is capable of listening on these sockets for changes and broadcasts,
which means that in this chapter we'll also be diving into JavaScript
Progrmamming. While Phoenix's web socket implementation work. The Phoenix
example code also deals very specifically with JavScript's ES2015 syntax, 
so we'll also take a little bit of time to very explain some of the syntaxes
that you'll see in a lot of code smaples for channels.

* What channels and topics are in Phoenix
* How to build your own channels and topics
* how to wrie the client code to interact with Phoenix
* How to begin writing your tests to cover channels
* An introduction on how to architect real-time web applications

Building channels and topics in Phoenix
----
The general idea is that for a real-time application to be albe to
accurately braodcast out every message to the right parties(and for it to be
able to understand messages that are sent in as well), Phoenix needs a
system for how the messages get in and out. This represents our channels.
Channels themselves are the phoenix-side of sockets; where the socket 
represents how the information is transferred between the client and the
server, the channel represents how the infromation this the server
and gets translated into something useful that Phoenix can work with.

Uderstanding sockets
----
The easiest way for us to start thinking aobut how real-time applcaitons
work in actural practice is to start building our own. Thorugh this we can
experiment and learn each of the moving pieces that are invloved with 
implementing the */channels*. If we open this file up we can get a good
picture for how to start constructing our sockets and channels.

Sockets are responsible for setting up exatly how a client can connect
and authenticate with the appropriate channels and topics. You can think
of sockets as the actual means of connecting the client to the channel and
topic(doing things like verifying the authentiation information, determining
how to route to sepcific channels and which channels are even available to
the Phoenix application, and some other tasks).

The second line in *user_socket.ex*(after the module definition)is this
line, use *Phoenix.Scoket*.

We'll use our IEx session to get a littel more information about what macros
and functionality this *use* statement provides to us. If we type in
`h Phoenix.Socket` in our IExsession we'll get a very helpful page back out 
that explains a lot of what some of the boilerplate code is and what each
line does. We also are provided some sample code that matches what we find
in `user_socket.ex` (without the comments). Per the ehlp documentation:

The next line that we need to pay attention to is commented out but 
important nonetheless:
```
## Channels
# channel "room:*", VocialWeb.RoomChannel
```
This line is where we would start building out each of the channels and
figure out how to start telling Phoenix about the existence of these
channels and which modules to map specific topics to.
This means any topic matching the pattern of `room:(something)` would
get mapped to the `RoomChannel`, where we would likely have some function
definitions with a pattern match something like this:
```
def join("room:" <> chat_room) do 
  #
end
```
Let's move on to the next lines in our code. This is the section that sets 
up what protocol our Phoenix application is going to be using for managing
/routing these long-lived sockets:
```
## Transports
transport :websocket, Phoenix.Transports.WebSocket
# transport :longpoll, Phoenix.Transports.LongPoll
```
Here you can see that we're relying on the web socket protocol for our 
Phoenix application. This is a pretty sound default that you'll use most
of the time unless you are working with perhaps legacy browsers or clients
that don't support web sockets but do support long-polling.

That line sitting in there allows you to very quickly change the mechanism
you want to use to deliver information to the client from the server. Again,
most of the time, you'll just want to stick with the web socket protocol in
general(and that's what we'll be using for the purpose of our Phoenix
application).

Next lien is the actual connection code that is responsible for telling both
the server and the client that the request by the client to subscirbe to 
the topic specified is a vlid request and that both the server and the
client can relying on that to send information: bidirectionally. By default,
we start with this code.
```
def connect(_params, socket) do 
  {:ok, socket}
end
```
All this does is tell Phoenix and the client that yes, this connection and 
subscription request is completely valid, no matter what (whether it 
should be valid or not). This might seem harmful but the reality is that
your web application may not actually need authentication for all parts of
its real-time communication. For example, we may want users to be able to 
view the results of plls live without crating a user for each poll that's
created. Either way, we're less concerned with actually checking the 
validity fo the connection and more concerned with establishing that
connection in the first place. It's also worth nothing that this does not
lock you into having to allow every subsription and join that may happen in
your application; you can set up separate sockets or separate channels/
topics that are private when we start talking about channels and the *join* function!


----- omit page ----

Understanding channels
----
As mentioned previously, channels are the in-between for sockets and topics.
They essentially act as the controllers of the Phoenix real-time application
puzzle. The eaiest way for us to really understand this is to drive right
into the code and start messing around with things until they break!


Making voting real-time
-----
We have our manual methods for voting, so we have a nice fallback should
we be working with a browser that doesn't support anything facny, but
let's also build the good stuff into our application! If we return back to
the JavaScript code that lives in `assets/js/socket.js`, we can go into
our conditional and start by adding a new bit of code by modifying our
current `join` code. Again, when we're building functionality we always
want to be building our functionality up from the simplest implemnetaion,
so we'll start with some dummy code!

Building our dummy functionality
-----


> Issue: when I added javaScript  into "index.html.eex"
> <i id="enable-polls-channel" data-poll-id="<%= @poll.id %>"></i>
> @poll causes a error "ArgumentError" something


Iproving Our Application and Adding Features
-----

* Image uploads for polls
* Voting restrictions so people can't vote in the same poll multiple times


### Desiging and implementing our new features
-----

For image uploads, we will need the following:
* Somewhere that we can store our images
* A way to link an image to a user or a poll
* A way to display the image as part of the poll

So what will we need to fulfill this criteria?
* Either a local file/directory or somewhere online to store images:
    * To keep things simple, we'll stick to using an uploads directory in our application instead of S3
* A new schema and database table to store images:
    * The images must be linked to polls
    * Images should also be linked to polls
    * We'll need a table on which to store the URL or path, the poll ID, the user ID, the alt text, and a caption
    * We shoudl have a maximum of one image per poll
* The image should be displayed on the actual page:
    * Where should the image be displayed?
    * The image should include the caption and the alt text
    * There must be a way to remove an image from a poll
    * Images should be deleted when a poll is deleted

### Working with uploads in Phoenix
----
We created a new directory in the root of our application called `uploads`.
We'll also want to make sure that the directory is included if we're using
something like Git for source control:
```
$ mkdir uploads
$ touch uploads/.gitkeep
```
We'll also have to mdoify our endpoint to allow serving files from this new
directory. To do this, you tell Phoenix that, as part of the plugs
responsible for keeping the server alive and moving, you'll also include a 
line for `Plug.Static` telling it that there is a new directory for it to
watch and serve out to the client appropriately.
```
plug Plug.Static, at: "/uploads", from: "./uploads"
```

We can quickly verify that our appliation is serving files.
```
$ echo 'Hello World' > uploads/test.txt
```
Then, open up your brower to `http://localhost:4000/uploads/test.txt`
and verify that when you load that URL, you get the text 'Hello World' back
in your browser!

### Adding file uploads to our new poll UI
----
We have a way for Phoenix to serve files out of that new directory, but we
don't have a way for any of that to actually get into our application! We'll
need to fix this bby adding a file-handling functionality into our
application. Open up `lib/vocial_web/templates/poll/new.html.eex` and add
the following code just following to the options input:
```
<label>
  Poll Image:
  <%= file_input f, :image %>
</label>
<br />
```
You'll also need to return back to the `form_for` call at the top of that
template and pass in the **multipart** option, set to `true`:
```
<%= form_for @poll, poll_path(@conn, :create), [multipart: true], fn f -> %>

```


### Writing the migration file
----
If we look back to our planned design for the uploads table, we came up
with the following criteria for what we figured we'd need: We'll need a
table to store the URL or path, the poll ID, the user ID, the alt text, 
and a caption. Let's create our migration and do all of the work the hard
way so that we can reinforce all of these concepts and continue to learn
how to build an Elixir and Phoenix app at a deep level! We'll start by
generating the migration and then editing that generated file:

```
$ mix ecto.gen.migration create_images_table
* creating priv/repo/migrations
* creating priv/repo/migrations/xxxxx_create_images_table.exs
```


### Modifying the schema and the context code
----





### Implementing voting restrictions
----
Right now, anybody on the internet can essentially stuff the ballot with as
many votes as our poor system will allow them to! this doesn't make for
very fair polls, so we'll take a naive approach and implement an IP-based
list of people who have already voted on each particular poll!

We'll need to start off by figuring out a way to identify the user's IP
address. The good news is that Phoenix already has that information ready
for us in the `conn` object! In the `conn` object, one of the keys in the
map is `remote_ip`, which is a tuple of the four octests in the IP address
of the connecting user! We can verify this either by placing an `IEx.pry`
statement in one of our controller actions and taking a look at the output 
for `conn.remote_ip`:
```
require IEx

def index(conn, _params) do 
  IEx.pry
  polls = Votes.list_polls()
  render conn, "index.html", polls: polls
end
```

```
Request to pry #PID<x.xxx.x> at ------

8: 
9: def index(conn, _params) do 
10: IEx.pry
11: 
12:

Allow? [Yn] y

------
pry(1)> conn.remote_ip
{xx.xxx.xxx.xxx}
```


### Creating the vote record migration
----
To do this, we'll need to add another table to our database. This new
table will have the xplicit purpose of recording votes for each poll,
which right now can just be something as simple as a list of IP addresses
that have voted on a particular poll. 
It's a pretty simple feature and design, so we'll keep the table structure
equally simple.

```
$ mix ecto.gen.migration add_vote_records_table
* creating priv/repo/migrations
* creating priv/repo/migartions/xxxx_add_vote_records_table.exs
```


Adding Chat to your Phoenix Application
-----

* Understanding a data model for a chat application
* Building the data model for a chat application
* Integrating chat with the data model into Phoenix
* Cleaning up the navigation layout of our application


### Working with the chat schema
----

table: messages
------
  id : integer, primary key
  message : string, not null
  author : string, not null
  poll_id : integer, references polls, nullable
  inserted_at : timestamp
  created_at : timestamp

### Fixing navigation in our application
-----

### Creating the chat UI
----

#### Building the UI Itself
----
Phoenix, unfortunately, does not have a built-in concept of shared 
templates or views, so we're going to have to make one ourself! We'll
start off by creating a new view called `VocialWeb.SharedView`.
Create `lib/vocial_web/views/shared_view.ex`



Using Presence and ETS in Phoenix
-----
We're now preparing to move into some of the more advanced topics
behind Elixir and Phoenix. A robust, perfomant application will also
utilize a lot of the more advanced language and framework features that
are generally behind the scenes of such applications. Two of these features
are Presence and **Erlang Term Storage(ETS)**.

* An introduction to Phoenix Presence
* How to utilize Phoenix Presence in your application
* How to implement the Elixir code for Presence
* How to implement the JavaScript code for Presence
* An introduction to ETS
* How to use ETS in an IEx window
* How to use ETS in Your Phoenix application
* Using ETS to store vote data
* Using ETS to store chat data
* Writing a fallback for ETS if there is no data

## Utilizing Presence and ETS to make our app moe robust
----
As we start scaling our application, we'll very quickly find that in Phoenix
applications , the bottleneck moves away from what our connections are doing
and how long they're sitting around for and moves toward the database.
In addtion, the concept of determining the state of connections(when they 
are established, how long they've been active, what their current state is, 
and so on) becomes a more important question to answer. The good news is 
that Elixir and Phoenix both have built-in tools that you get for free that
allow you to solve these problems!
Elixir provides access to ETS through the :ets namespace. This allows you to
create tables, insert/retrieve/delete data, and so on. ETS specifically
stores any information you enter into it in memory, which means any access to it is incredibly fast!

### What is Presence?
----
We have already briefly talked about Presence, but there's actually a lot
more to it. Presence works through the concept of **conflict-free replicated data types(CRDTs)**.
The general premise behind Presence and how it was built is that, to
actually be able to track users as they join/leave/go away, you need to be
able to track the state change over time and resolve that change with other 
servers as well, since most applications today are clustered in some way.
The difficulty comes from when you have multiple servers that are all in
charge of recording a user's state. If you have three serers and one goes
down, and another server records a user signing off, and then another
server records the user signing on, which one wins?


### Updating our char UI
----


### JavaScript implementation
----
One of the first things that we'll want to do is install **jQuery** for us
to use as part of the JavaScript code we'll need to write for Presence. 
We'll be doing a good amount of DOM manipulation, and that's the kind of
thing that is complicated to do without something like jQuery beind it.
To install a new library, we'll need to open up a terminal to our project
root, and then move into the *assets/* directory. 
```
$ npm install --save jquery
```



Using ETS
----
ETS is an in-memory storage system that stores pretty simple sets of 
information. ETS is insanely fast; imagine if every web application you ever
wrote had something like Redis attached to it by default, for free, without
any additional infrastructure! It's a very cool, but also often underutilized,
feature that is rooted even deeper than Elixir itself. It can be used to 
sotre any data structure that Elixir and Erlang themselves can store.
ETS works on the princple that each process itself can own up 1,400 tables
of information. However, this also means that all of the information in 
those tables goes away when that process goes away!

Why use ETS?
-----
The first reason is that it's fast! It's also very simple to implement and
requires no additional infrastructure, which means if you need to cache
temporary information, it's actually very simple to do so. Since this
information is so incredibly temporary, howeber, we need to make sure that
we don't store any information in it that is actually necessary to the function of our application, or at the very least information that is not also
available somewhere in a more permanent form of storage.

That is honestly where ETS shines: using it for temporary caches of 
information that you have elsewhere, but maybe more expensive to fetch
from. When you can do that, you can speed up your application pretty
dramatically! That being said, this is also a cache, which means it carries
with it a certain set of caveats:

* This data is transient -- don't expect it to always be there
* Don't cache prematurely! This is proviced as an example of how to cache
  via ETS, but that doesn't mean you should rush to do this!
* The data stored n ETS goes away when the process that created it goes away!

Experimenting with ETS in an IEx window
----

```
iex> table = :ets.new(:test_data, [:set])

iex> :ets.insert(table, {:key1, "value"})
true
iex> :ets.lookup(table, :key1)
[key1: "value"]
```

* :set:  This is the default type for tables; you are allowed to enter
   in one value per key, and all keys must be unique
* :ordered_set: similar to `:set`, but the set itself is ordered by the keys
* :bag: You can have multiple items per key, but the items have to be unique(per key)
* :duplicate_bag: Similar to a regular bag, but the items do not have to be unique anymore

You can also control who else(that is, which another process) is allowed to 
access the information in these tables. The three options available to you
as follows:

* public: 
* protected:
* private:

```
iex(6)> :ets.new(:test, [:bag, :named_table])
:test
iex(7)> :ets.insert(:test, {:user1, "Brandon"})
true
iex(8)> :ets.insert(:test, {:user1, "Richery"})
true
iex(9)> :ets.lookup(:test, :user1)
[user1: "Brandon", user1: "Richery"]

```
Here, you can see that we used the `:bag` table type, so we can store lots
of different information in our bag, but no duplicates. If we try, nothing happen:
```
iex(6)> :ets.new(:test, [:bag, :named_table])
:test
iex(7)> :ets.insert(:test, {:user1, "Brandon"})
true
iex(8)> :ets.insert(:test, {:user1, "Richery"})
true
iex(9)> :ets.lookup(:test, :user1)
[user1: "Brandon", user1: "Richery"]
iex(10)>
iex(11)> :ets.insert(:test, {:user1, "Brandon"})
true
iex(12)> :ets.lookup(:test, :user1)
[user1: "Brandon", user1: "Richery"]


iex(13)> :ets.delete(:test, :user1)
true
iex(14)> :ets.lookup(:test, :user1)
[]
```

ETS table and GenServer
----
Let's start off with an ETS table that can store information about the various
cahnges in Presence over the course of its lifetime.
We'll need to wrap this inside of a GenServer, since we need a process that
will live with our server rather than with each request(remember that ETS
tables only live as long as their owner). Don't worry too much about the
GenServer conventions at play here;

