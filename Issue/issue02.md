** RESOLVED **

issue:
   when I access votes/new or poll/new without login, alert window "must login" doesn't come up to redirect index automatically.
   votes/new -> /index directly.
    put_flash(:error, "You must be logged in to do that!")
    --> alert window should show up

result: app.html.eex 
      <p class="alert alert-danger" role="alert"><%= get_flash(@conn, :error) %></p>
this has been deleted although I don't know.

