defmodule Vocial.Votes do 
  @moduledoc """
	We'll want to import Ecto.query so we can build out any special
	queries that may be required. We'll also want to alias in our 
	Repo, Poll, and Option modules.
	We have a nice, handy, single interface to grab data out of
	the database, we should start by writing something that does 
	precisely that Let's grab a list of all of the Polls out of
	database(and make sure we include our Options with it).	

	"""
  import Ecto.Query, warn: false

	alias Vocial.Repo
	alias Vocial.Votes.Poll
	alias Vocial.Votes.Option
	alias Vocial.Votes.Image 
	alias Vocial.Votes.VoteRecord
	alias Vocial.Votes.Message 

	@doc """
	This can provide a simple interface to get the data out of the
	system and to our controllers. this simple function is going to 
	preload the Options for those Polls as well.
	"""

 	def list_polls do 
 		Repo.all(Poll) |> Repo.preload([:options, :image, :vote_records, :messages])
 	end
	@doc """
	This create a blank changeset for us to start working with,.
	"""
 	def new_poll do 
 		Poll.changeset(%Poll{}, %{})
 	end
 	@doc """
  We're expecting to see both a poll and an options parameter
  be passed in. This will take in the poll parameters and a list
  of options to add to the poll.
  We'll then use the with statement to make sure the results we
  get from that function make sense. If all is good, we'll send
  a flash message out and redirect back to the list of polls

  We'll wrap the entire thing inside of a transaction first.
  The Repo.transaction statement takes in a function as its 
  only argument and expects either the operation to complete
  or a rollback to be issued.
 	"""
 	def create_poll_with_options(poll_attrs, options, image_data \\ nil) do 
 		#IO.puts "poll_attrs=>#{inspect poll_attrs},\n options=>#{inspect options}"
 		Repo.transaction(fn -> 
 			with {:ok, poll} <- create_poll(poll_attrs),
 					 {:ok, _options} <- create_options(options, poll),
 					 {:ok, filename} <- upload_file(poll_attrs, poll),
 					 {:ok, _upload}  <- save_upload(poll, image_data, filename)
 			do 
 				#poll |> Repo.preload(:options)
 				poll |> Repo.preload([:options, :image, :vote_records])
 			else 
 				_ -> Repo.rollback("Failed to create poll")
 			end 
 		end)
 	end 
 	defp upload_file(%{"image" => image, "user_id" => user_id}, poll) do 
 		extension = Path.extname(image.filename)
 		filename = "#{user_id}-#{poll.id}-image#{extension}"
 		File.cp(image.path, "./uploads/#{filename}")
 		{:ok, filename}
 	end 
 	defp upload_file(_,_), do: {:ok, nil}

 	defp save_upload(_poll, _image_data, nil), do: {:ok, nil}
 	defp save_upload(poll,
 									 %{"caption" => caption, "alt_text" => alt_text},
 									 filename )
 	do 
 		attrs = %{
 			url: "/uploads/#{filename}",
 			alt: alt_text,
 			caption: caption,
 			poll_id: poll.id,
 			user_id: poll.user_id
 		}
 		IO.puts "** Image ** attrs=>#{inspect attrs}"
 		%Image{}
 		|> Image.changeset(attrs)
 		|> Repo.insert()
 	end 

	@doc """
	This just starts with a blank Poll struct, passes that into
	the changeset function on the Poll schema, and then finally
	inserts the whole thing to Repo.Repo.insert(). If it is
	successful, it will return data in the format 
	{:ok, returned_struct_from_db}:

	"""
 	def create_poll(attrs) do 
 		%Poll{}
 		|> Poll.changeset(attrs)
 		|> Repo.insert()
 	end 
 	@doc """
	This will iterate over a list of options, add the poll_id
	to each poll title, and create each poll. If fail, the
	whole thing should fail and rollback the transaction.
	"""
 	def create_options(options, poll) do 
 		results = Enum.map(options, fn option ->
 			create_option(%{title: option, poll_id: poll.id})
 		end)

 		if Enum.any?(results, 
 									fn {status, _} -> status == :error end) do 
 			{:error, "Failed to create an option"}
 		else
 			{:ok, results}
 		end 
 	end 
 	def create_option(attrs) do 
 		%Option{}
 		|> Option.changeset(attrs)
 		|> Repo.insert()
 	end

 	def list_options do
 		Repo.all(Option) |> Repo.preload(:poll)
 	end

 	def vote_on_option(option_id, voter_ip) do 
 		with option <- Repo.get!(Option, option_id),
 				 false <- already_voted?(option.poll_id, voter_ip),
 				 votes <- option.votes + 1,
 				 {:ok, option} <- update_option(option, %{votes: votes}),
 				 {:ok, _vote_record} <- record_vote(%{ poll_id: option.poll_id, ip_address: voter_ip})
 		do 
 			update_option(option, %{votes: votes})
 		else 
 			_ -> {:error, "Could not place vote"}
 		end
 	end
 	def update_option(option, attrs) do 
 		option
 		|> Option.changeset(attrs)
 		|> Repo.update()
 	end 
 	def record_vote(%{poll_id: _poll_id, ip_address: _ip_address}=attrs) do 
 		%VoteRecord{}
 		|> VoteRecord.changeset(attrs)
 		|> Repo.insert()
 	end 


  def get_poll(id) do
  	Repo.get!(Poll, id) |> Repo.preload([:options, :image, :vote_records, :messages])
  end

  def already_voted?(poll_id, ip_address) do 
  	votes = (from vr in VoteRecord,
  						 where:
  						 	 vr.poll_id == ^poll_id and
  						 	 vr.ip_address == ^ip_address)
  					|> Repo.aggregate(:count, :id)
  	votes > 0
  end 

  def list_lobby_messages do 
  	Repo.all(
  		from m in Message,
  		where: is_nil(m.poll_id),
  		order_by: [desc: :inserted_at],
  		limit: 100
  		)
  end

  def list_poll_messages(poll_id) do 
  	Repo.all(
  		from m in Message,
  		where: m.poll_id == ^poll_id,
  		order_by: [desc: :inserted_at],
  		limit: 100,
  		preload: [:poll]
  		)
  end 
  def create_message(attrs) do 
  	%Message{}
  	|> Message.changeset(attrs)
  	|> Repo.insert() 
  end 

end 